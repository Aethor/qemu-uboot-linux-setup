ARCH=arm
CROSS_COMPILE=arm-linux-gnueabi-
CORE_NUMBERS=4


all: linux uboot 

linux: 
	export ARCH=$(ARCH); \
	export CROSS_COMPILE=$(CROSS_COMPILE); \
	cd linux-5.6.3/; \
	make -j$(CORE_NUMBERS); \
	cp ./arch/arm/boot/zImage ../tftpboot/kernel; \
	cp ./arch/arm/boot/dts/vexpress-v2p-ca9.dtb ../tftpboot/vexpress.dtb; \
	chmod 666 ../tftpboot/kernel; \
	chmod 666 ../tftpboot/vexpress.dtb

uboot: 
	export ARCH=$(ARCH); \
	export CROSS_COMPILE=$(CROSS_COMPILE); \
	cd u-boot-master/; \
	make -j$(CORE_NUMBERS); \

