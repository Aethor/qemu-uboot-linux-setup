#!/bin/perl

use File::Spec;
use File::Path;
use File::Basename;


my $rootfsDir = File::Spec->rel2abs("./rootfs");
File::Path::make_path($rootfsDir);
my $busyboxPath = File::Spec->catdir($rootfsDir, "/bin/busybox");

`mkdir -pv ./rootfs/{bin,dev,sbin,etc,proc,sys/kernel/debug,usr/{bin,sbin},lib,lib64,mnt/root,root}`;
`sudo cp -av /dev/{null,console,tty,tty1,tty2,tty3,tty4,sda1} ./rootfs/dev/`;
system "cp", "./busybox", $busyboxPath;
system "chmod", "777", $busyboxPath;

my @binPaths = `busybox --list-full`;
for (@binPaths) {
  chomp;
  my $absBinPath = File::Spec->rel2abs(File::Spec->catdir($rootfsDir, dirname($_)));
  my $relDir = File::Spec->abs2rel($busyboxPath, $absBinPath);
  $relDir = "./" . $relDir if substr($relDir, 0, 1) ne ".";
  system "ln", "-s", $relDir, File::Spec->catdir($rootfsDir, $_);
}

`virt-make-fs --partition --format=qcow2 --type=ext2 rootfs rootfs.img --size 50M`;
