#!/bin/sh

wget 'https://gitlab.denx.de/u-boot/u-boot/-/archive/master/u-boot-master.zip'
unzip u-boot-master.zip

wget 'https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.6.3.tar.xz'
tar -xvf 'linux-5.6.3.tar.xz'

wget 'https://busybox.net/downloads/binaries/1.31.0-defconfig-multiarch-musl/busybox-armv7r' -O busybox
chhmod 777 busybox 
