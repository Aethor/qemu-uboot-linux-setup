#!/bin/sh

mkdir tftpboot
chmod 767 tftpboot
cp ./linux-5.6.3/arch/arm/boot/zImage tftpboot/kernel 
cp ./linux-5.6.3/arch/arm/boot/dts/vexpress-v2p-ca9.dtb tftpboot/vexpress.dtb
chmod 666 tftpboot/kernel
chmod 666 tftpboot/vexpress.dtb
